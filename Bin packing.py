""" Projet 1 : Bin Packing """

__author__ = "Noemie Muller"
__matricule__ = "000458865"
__date__ = "19/03/2018"
__cours__ = "info_f-103"
__titulaire__ = "Jérôme De Boeck"

import sys
import time

def bin_packing(file):
	"""
	\brief  Donne la combinaison optimale pour ranger x objets dans un minimum de boites
	\param	Fichier où sont indiqués les tailles des objets à placer ainsi que la taille 
				des boites
	\return None
	"""

	fichier = open(file) 
	volumeMax = int(fichier.readline().strip()) #1e ligne du fichier = taille des boites
	objects = [int(x) for x in fichier.readlines()[0:]] #autres lignes = tailles des objets
	fichier.close()
	objects.sort()
	objects.reverse()
	
	print("Capacité des boites :",volumeMax)
	print("Nombre d'objets :",len(objects))
	print("Poids des objets :",",".join(str(i) for i in objects))
	print("Résolution...")

	start = time.time()
	objectPlacement(objects,volumeMax)
	end = time.time()
	print("Temps de résolution :",str(end-start),'secondes')

def objectPlacement(objects,volumeMax,boites=[]):
	if len(objects) == 0 : #la liste est vide donc tous les objets ont été placés
		displayBoxes(boites) #on print la solution
	else :
		currentBox = 0
		while currentBox < len(boites) and fullBox(boites,currentBox,objects,volumeMax):
			"""boucle qui parcourt toutes les boites et s'arrête quand elle en trouve 
			une avec assez de place ou en crée une nouvelle"""
			currentBox += 1
		if currentBox == len(boites) :
			boites.append([objects.pop(0)]) #crée une nouvelle boite et y place l'objet
			objectPlacement(objects,volumeMax,boites)
		else :
			boites[currentBox].append(objects.pop(0)) #place l'objet dans la boite trouvée
			objectPlacement(objects,volumeMax,boites)

def fullBox(boites,currentBox,objects,volumeMax):
	"""
	\brief  Vérifie que la boite pointée a de la place pour l'objet à placer
	\param	Liste des contenus des boites, indice de la boite pointée, liste des objets à placer
	\return Bool (vrai si la boite est remplie)
	"""
	return sum(boites[currentBox])+objects[0] > volumeMax

def displayBoxes(boites):
	"""
	\brief  Print la meilleure combinaison trouvée
	\param	Liste : chaque elem = 1 boite = liste des poids des objets contenus
	\return None
	"""
	print("Meilleure solution : \n{} boites".format(len(boites)))
	for i in range(len(boites)):
		print("Boite {} :".format(i+1),' '.join(str(x) for x in boites[i]),'; total :',sum(boites[i]))

if __name__ == "__main__" :
	try :
		file = sys.argv[1]
		bin_packing(file)
	except IOError :
		print("Erreur : le fichier '{}' est introuvable.".format(file))
		sys.exit()